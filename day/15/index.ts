"use strict";

function hashSum(input: string[]) {
  return input
    .flatMap((row) => row.split(","))
    .map((str) => hash(str))
    .reduce((total, value) => total + value);
}

type Lens = string;

function hashMapSum(input: string[]) {
  let boxes: Lens[][] = new Array(256);
  input
    .flatMap((row) => row.split(","))
    .map((op) => op.split(/([=\-])|(-)+[=-]+/g))
    .map((op) => op.filter((o) => o !== undefined))
    .map((op) => ({
      operation: op[1],
      box: hash(op[0]),
      label: op[0],
      lens: `${op[0]} ${op[2]}`,
    }))
    .map((step, index) => {
      if (!boxes[step.box]) {
        boxes[step.box] = [];
      }
      if (step.operation === "=") {
        const index = boxes[step.box].findIndex(
          (lens) => lens.indexOf(step.label) !== -1
        );
        if (index !== -1) {
          boxes[step.box].splice(index, 1, step.lens);
        } else {
          boxes[step.box].push(step.lens);
        }
      } else {
        const index = boxes[step.box].findIndex(
          (lens) => lens.indexOf(step.label) !== -1
        );
        if (index !== -1) {
          boxes[step.box].splice(index, 1);
        }
      }
    });
  return boxes.reduce(
    (total, box, boxIndex) =>
      total +
      box.reduce(
        (total, op, opIndex) =>
          total + (boxIndex + 1) * (opIndex + 1) * parseInt(op.split(" ")[1]),
        0
      ),
    0
  );
}

const hash = (str: string) =>
  str
    .split("")
    .reduce((total, char) => ((total + char.charCodeAt(0)) * 17) % 256, 0);

export { hashSum, hashMapSum };
