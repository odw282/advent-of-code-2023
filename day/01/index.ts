"use strict";

const digits = [
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
];
function calibrate(
  input: string[],
  mapFunc: (line: string[]) => string[] = (line) =>
    line.filter((char) => parseInt(char))
) {
  return input
    .map((line) => line.split(""))
    .map(mapFunc)
    .map((line) => parseInt(`${line[0]}${line[line.length - 1]}`))
    .reduce((total, value) => total + value, 0);
}

function calibrateDigits(input: string[]) {
  const mapDigits = (line: string[]) => {
    const output: string[] = [];
    line.reduce((buffer, char, index) => {
      buffer = buffer.concat(char);
      const parsed = parseInt(char);
      const isDigit = digits.findIndex((digit) => buffer.indexOf(digit) !== -1);
      if (isFinite(parsed)) {
        output.push(parsed.toString());
        buffer = "";
      } else if (isDigit !== -1) {
        output.push((isDigit + 1).toString());
        buffer = char;
      }
      return buffer;
    }, "");
    return output;
  };

  return calibrate(input, mapDigits);
}

export { calibrate, calibrateDigits };
