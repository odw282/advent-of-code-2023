'use strict';
function lowestLocation(input: string[]) {
  let {seeds, blocks} = parseAlmanac(input);

  for(const block of blocks) {
    const mapped = [];
    for(const seed of seeds) {
      mapped.push(mapSeed(seed, block));
    }
    seeds = mapped;
  }
  return seeds.sort()[0];
}

function mapSeed( seed: number, ranges: number[][]) {
  for(const [dest, src, range] of ranges) {
    if (src <= seed && seed < src + range) {
      return seed - src + dest;
    }
  }
  return seed;
}


function parseAlmanac(input: string[]) {
  const seeds = input.shift().split(": ")[1].trim().split(/\s+/).map(i => parseInt(i.trim()));
  const blocks = input.map(block => {
    const [, ...maps] = block.split("\n");
    return maps.map(m => m.trim().split(/\s+/).map(i => parseInt(i.trim())));
  })
  return {
    seeds,
    blocks
  }
}
///


/**
 * Returns the destination of the map range.
 */
const rDest = (range: number[]) => range[0];

/**
 * Returns the (source) start of the map range.
 */
const rStart = (range: number[]) => range[1];

/**
 * Returns the (source) end of the map range
 */
const rEnd = (range: number[]) => range[2];
/**
 * Apply the map range translation to the value
 */
const rTranslate = (x: number, r: number[]) => x - rStart(r) + rDest(r);

/**
 * Compares the value to the map range and returns an integer indicating the sort order.
 */
const rCompare = (x: number, r: number[]) => {
  if (x < rStart(r)) {
    return -1;
  } else if (x >= rEnd(r)) {
    return 1;
  }
  return 0;
};


/**
 * Searches the array for an item.
 * @param {Array} arr
 * @param {Any} item - The item to search for.
 * @param {(any) => number} compareFn - Function used to compare items.
 *    Expected to return:
 *      - Less than zero if search value is less than current item
 *      - Greater than zero if search value is greater than item
 *      - Zero if search value is equal to item.
 * @returns {number} The index of the item (if found), otherwise (-(insertion point) -1)
 */
export const binarySearch = (arr: any[], item: any, compareFn: (x:any, arr: any[]) => number): number => {
  let l = 0;
  let u = arr.length - 1;
  while (l <= u) {
    const m = (l + u) >> 1;
    const comp = compareFn(item, arr[m]);
    if (comp < 0) {
      u = m - 1;
    } else if (comp > 0) {
      l = m + 1;
    } else {
      return m;
    }
  }
  return ~l;
};

const levelOne = (lines: string[]) => {
  const { seeds, blocks } = parseAlmanac(lines);

  console.log(seeds, blocks);
  // translate the seed position by running it through the almanacs maps.
  const seedPosition = (x:number) =>
      blocks.reduce((current, ranges) => {
        const index = binarySearch(ranges, current, rCompare);
        return index >= 0 ? rTranslate(current, ranges[index]) : current;
      }, x);

  console.log(seeds.map(seedPosition))

  return Math.min(...seeds.map(seedPosition));
};



export function parseSeeds(input: string[]){
  return input[0].split(":")[1].split(" ").map( s => parseInt(s.trim())).filter(n => n)
}

class Mapper{
  public ranges: any[] = []

  public addRange(r: any){
    this.ranges.push(r)
  }
  public transform(input: number){
    // if we didn't find a match within the ranges.. then
    // the output value is the same as the input value
    let output = input

    // for each range, check if the input falls within the source range
    for (const r of this.ranges){
      let destinationStart = r[0]
      let sourceStart = r[1]
      let range = r[2]
      if ( sourceStart <= input && input <= sourceStart + range - 1){
        let delta = destinationStart - sourceStart
        output = input + delta
        break
      }
    }

    return output
  }
}

export function parseMappers(input: any[]){
  let m = []
  var mapper: Mapper
  input.slice(1,).forEach( line => {
    if (line == ''){
      // empty line indicates end of mapper definition
      // so if mapper is defined, push it
      mapper && m.push(mapper)
      mapper = new Mapper()
    }

    // line is numbers separated by spaces
    if (/(\d+\s)+/.test(line)){
      let range = line.split(" ").map((n: string) => parseInt(n))
      mapper.addRange(range)
    }
  })
  if (mapper){
    m.push(mapper)
  }
  return m
}

export function calculateLocation(mappers: any[], seed: number){
  return mappers.reduce( (location, m: Mapper) => m.transform(location), seed)
}

export function tryThis(input: any[]) {
  let seeds = parseSeeds(input)
  let mappers = parseMappers(input)
  let minLocation = Math.min(...seeds.map( s => calculateLocation(mappers, s)))
  return minLocation
}

// 1272039621

export {
  lowestLocation,
    levelOne
};
