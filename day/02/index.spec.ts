import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {gamesPossible, setPower} from "./index";

const games = readFileIntoArray(path.join(__dirname, "./input.txt"));
describe("Day 2: Cube Conundrum", () => {

    test("Test Case A", () => {
      const answer = gamesPossible([
          "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
          "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
          "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
          "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
          "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
      ]);
      expect(answer).toEqual(8);
    });

    test("Solution A", () => {
      const answer = gamesPossible(games);
      expect(answer).toEqual(2810);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = setPower([
          "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green",
          "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue",
          "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red",
          "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red",
          "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"
      ])
      expect(answer).toEqual(2286);
    });

    it("Solution B", () => {
      const answer = setPower(games);
      expect(answer).toEqual(69110);
      console.info("B:", answer);
    });

});
