import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {sumOfInfinite, sumOfLength} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
  "...#......",
  ".......#..",
  "#.........",
  "..........",
  "......#...",
  ".#........",
  ".........#",
  "..........",
  ".......#..",
  "#...#.....",
];

describe("Day 11: Cosmic Expansion", () => {
  test("Test Case A", () => {
    const answer = sumOfLength(testInput);
    expect(answer).toEqual(374);
  });

  test("Solution A", () => {
    const answer = sumOfLength(input);
    expect(answer).toEqual(9609130);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = sumOfInfinite(testInput)
    expect(answer).toEqual(82000210);
  });

  it("Solution B", () => {
    const answer = sumOfInfinite(input);
    expect(answer).toEqual(702152204842);
    console.info("B:", answer);
  });
});
