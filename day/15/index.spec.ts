import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {hashMapSum, hashSum} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
    "rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7"
]
describe("Day 15: Lens Library", () => {

    test("Test Case A", () => {
      const answer = hashSum(testInput)
      expect(answer).toEqual(1320);
    });

    test("Solution A", () => {
      const answer = hashSum(input)
      expect(answer).toEqual(521341);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = hashMapSum(testInput)
      expect(answer).toEqual(145);
    });

    it("Solution B", () => {
      const answer = hashMapSum(input)
      expect(answer).toEqual(252782);
      console.info("B:", answer);
    });

});
