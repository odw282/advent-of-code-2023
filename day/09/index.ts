"use strict";
import { parseInt } from "lodash";
import * as process from "process";

function predict(input: string[]) {
  return input
    .map((row) => row.split(/\s+/).map((i) => parseInt(i)))
    .map(reduceDifference)
    .map(extrapolateForward)
    .reduce((total, value) => total + value);
}

function predictBackwards(input: string[]) {
  return input
      .map((row) => row.split(/\s+/).map((i) => parseInt(i)))
      .map(reduceDifference)
      .map(extrapolateBackwards)
      .reduce((total, value) => total + value);
}

const reduceDifference = (sequence: number[]) => {
  let nextSequence = sequence;
  const sequences: number[][] = [nextSequence];
  while (true) {
    nextSequence = difference(nextSequence);
    sequences.push(nextSequence);
    if (isAllZeros(sequences[sequences.length - 1])) {
      break;
    }

    if (sequences.length > 100) {
      process.exit(0);
    }
  }
  return sequences;
}

const extrapolateForward = (sequences: number[][]) => {
  sequences.reverse().map((sequence, index) => {
    // Second row
    if (sequences[index - 1]) {
      sequence.push(
          sequence[sequence.length - 1] +
          sequences[index - 1][sequence.length - 1]
      );
    } else {
      // add the zero
      sequence.push(sequence[sequence.length - 1]);
    }
  });
  return sequences[sequences.length - 1].reverse()[0];
}


const extrapolateBackwards = (sequences: number[][]) => {
  sequences.reverse().map((sequence, index) => {
    // Second row
    if (sequences[index - 1]) {
      sequence.unshift(
          sequence[0] -
          sequences[index - 1][0]
      );
    } else {
      // add the zero
      sequence.unshift(sequence[0]);
    }
  });
  return sequences.reverse()[0][0];
  //return sequences[sequences.length - 1].reverse()[0];
}

const difference = (sequence: number[]) => {
  const diff: number[] = [];
  for (let i = 0; i < sequence.length - 1; i++) {
    diff.push(sequence[i + 1] - sequence[i]);
  }
  return diff;
};

const isAllZeros = (sequence: number[]) => sequence.every((n) => n === 0);

export { predict, predictBackwards };
