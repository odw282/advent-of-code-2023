import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { sumReflections, sumReflectionsAlt } from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"), "\n\n");
const testInput = [
  `#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.`,
  `#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#`,
];
describe("Day 13: Point of Incidence", () => {
  test("Test Case A", () => {
    const answer = sumReflections(testInput);
    expect(answer).toEqual(405);
  });

  test("Solution A", () => {
    const answer = sumReflections(input);
    expect(answer).toEqual(34100);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = sumReflectionsAlt(testInput);
    expect(answer).toEqual(400);
  });

  it("Solution B", () => {
    const answer = sumReflectionsAlt(input);
    expect(answer).toEqual(33106);
    console.info("B:", answer);
  });
});
