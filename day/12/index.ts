"use strict";
import {memoize, parseInt} from "lodash";

function possibleSprings(input: string[]) {
  return parse(input)
    .map(([a, b]) => findPossibilities(a, b))
    .reduce((total, value) => total + value);
}

function possibleExpandedSprings(input: string[]) {
  return parse(input)
      .map(expand)
      .map(([a, b]) => findPossibilities(a, b))
      .reduce((total, value) => total + value);
}

const CACHE = new Map<string, number>();

function findPossibilities(springs: string[], sequence: number[]) {
  if (springs.length === 0) {
    return sequence.length === 0 ? 1 : 0;
  }

  if (sequence.length === 0) {
    return springs.includes("#") ? 0 : 1;
  }

  const KEY = springs.join("").concat("_").concat(sequence.join(","));
  if (CACHE.has(KEY)) {
   return CACHE.get(KEY);
  }

  let total = 0;

  if (springs[0] === "." || springs[0] === "?") {
    total += findPossibilities(springs.slice(1), sequence);
  }

  if (springs[0] === "#" || springs[0] === "?") {
    if (
      sequence[0] <= springs.length &&
      !springs.slice(0, sequence[0]).includes(".") &&
      (sequence[0] === springs.length || springs.slice(sequence[0])[0] !== "#")
    ) {
      total += findPossibilities(
        springs.slice(sequence[0] + 1),
        sequence.slice(1)
      );
    }
  }
  CACHE.set(KEY, total);
  return total;
}

function parse(input: string[]) {
  return input.map((row): [string[], number[]] => {
    const [springs, sequence] = row.split(" ");
    return [springs.split(""), sequence.split(",").map((i) => parseInt(i))];
  });
}

function expand(data: [string[], number[]] ): [string[], number[]]  {
  const [springs, sequence] = data
  let expanded: [string[], number[]] = [springs.slice(), sequence.slice()];
  for (let i = 0; i < 4; i++) {
    expanded[0] = expanded[0].concat(["?", ...springs]);
    expanded[1] = expanded[1].concat([...sequence]);
  }
  return expanded;
}

export { possibleSprings, possibleExpandedSprings };
