import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {levelOne, lowestLocation, tryThis} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"), "\n", false);
const testInput = [
  "seeds: 79 14 55 13",

  `seed-to-soil map:
  50 98 2
  52 50 48`,

  `soil-to-fertilizer map:
   0 15 37
   37 52 2
   39 0 15`,

  `fertilizer-to-water map:
   49 53 8
   0 11 42
   42 0 7
   57 7 4`,

  `water-to-light map:
   88 18 7
   18 25 70`,

  `light-to-temperature map:
   45 77 23
   81 45 19
   68 64 13`,

  `temperature-to-humidity map:
   0 69 1
   1 0 69`,

  `humidity-to-location map:
   60 56 37
   56 93 4`,
];
const testInput2 = readFileIntoArray(path.join(__dirname, "./test.txt"), "\n", false);


describe("Day 5: If You Give A Seed A Fertilizer", () => {
  // test("Test Case A", () => {
  //   const answer = lowestLocation(testInput2);
  //   expect(answer).toEqual(35);
  // });


  test("Solution A", () => {
    const answer = tryThis(input);
    expect(answer).toEqual(32);
    console.info("A:", answer);
  });


  test("Solution A2", () => {
    const answer = tryThis(testInput2);
    expect(answer).toEqual(35);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = "A";
    expect(answer).toEqual("A");
  });

  it("Solution B", () => {
    const answer = "B";
    expect(answer).toEqual("B");
    console.info("B:", answer);
  });
});
