import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { getMaxDistance, totalEnclosed } from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = ["..F7.", ".FJ|.", "SJ.L7", "|F--J", "LJ..."];

const testInput2 = [
  ".F----7F7F7F7F-7....",
  ".|F--7||||||||FJ....",
  ".||.FJ||||||||L7....",
  "FJL7L7LJLJ||LJ.L-7..",
  "L--J.L7...LJS7F-7L7.",
  "....F-J..F7FJ|L7L7L7",
  "....L7.F7||L7|.L7L7|",
  ".....|FJLJ|FJ|F7|.LJ",
  "....FJL-7.||.||||...",
  "....L---J.LJ.LJLJ...",
];

const testInput3 = [
  "FF7FSF7F7F7F7F7F---7",
  "L|LJ||||||||||||F--J",
  "FL-7LJLJ||||||LJL-77",
  "F--JF--7||LJLJ7F7FJ-",
  "L---JF-JLJ.||-FJLJJ7",
  "|F|F-JF---7F7-L7L|7|",
  "|FFJF7L7F-JF7|JL---7",
  "7-L-JL7||F7|L7F-7F7|",
  "L.L7LFJ|||||FJL7||LJ",
  "L7JLJL-JLJLJL--JLJ.L",
];

const testInput4 = [
  "..........",
  ".S------7.",
  ".|F----7|.",
  ".||....||.",
  ".||....||.",
  ".|L-7F-J|.",
  ".|..||..|.",
  ".L--JL--J.",
  "..........",
];

const testInput5 = [
  "...........",
  ".S-------7.",
  ".|F-----7|.",
  ".||.....||.",
  ".||.....||.",
  ".|L-7.F-J|.",
  ".|..|.|..|.",
  ".L--J.L--J.",
  "...........",
];

describe("Day 10: Pipe Maze", () => {
  test("Test Case A", () => {
    const answer = getMaxDistance(testInput);
    expect(answer).toEqual(8);
  });

  test("Solution A", () => {
    const answer = getMaxDistance(input);
    expect(answer).toEqual(7005);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = totalEnclosed(testInput2);
    expect(answer).toEqual(8);
  });

  it("Test Case B2", () => {
    const answer = totalEnclosed(testInput3);
    expect(answer).toEqual(10);
  });

  it("Test Case B3", () => {
    const answer = totalEnclosed(testInput4);
    expect(answer).toEqual(4);
  });

  it("Test Case B4", () => {
    const answer = totalEnclosed(testInput5);
    expect(answer).toEqual(4);
  });

  it("Solution B", () => {
    const answer = totalEnclosed(input);
    expect(answer).toEqual(417);
  });
});
