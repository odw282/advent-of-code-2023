import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {possibleExpandedSprings, possibleSprings} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
"???.### 1,1,3",
".??..??...?##. 1,1,3",
"?#?#?#?#?#?#?#? 1,3,1,6",
"????.#...#... 4,1,1",
"????.######..#####. 1,6,5",
"?###???????? 3,2,1"
];

describe("Day 12: Hot Springs", () => {

    test("Test Case A", () => {
      const answer = possibleSprings(testInput);
      expect(answer).toEqual(21);
    });

    test("Solution A", () => {
      const answer = possibleSprings(input);
      expect(answer).toEqual(7163);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = possibleExpandedSprings(testInput)
      expect(answer).toEqual(525152);
    });
    //
    it("Solution B", () => {
      const answer = possibleExpandedSprings(input)
      expect(answer).toEqual(17788038834112);
      console.info("B:", answer);
    });

});
