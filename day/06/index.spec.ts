import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {boatRace, singleRace} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = ["Time:      7  15   30", "Distance:  9  40  200"];

describe("Day 6: Wait For It", () => {
  test("Test Case A", () => {
    const answer = boatRace(testInput);
    expect(answer).toEqual(288);
  });

  test("Solution A", () => {
    const answer = boatRace(input);
    expect(answer).toEqual(2065338);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = singleRace(testInput)
    expect(answer).toEqual(71503);
  });

  it("Solution B", () => {
    const answer = singleRace(input)
    expect(answer).toEqual(34934171);
    console.info("B:", answer);
  });
});
