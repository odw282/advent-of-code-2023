"use strict";

type Card = {
  card: string;
  winning: number[];
  available: number[];
};

function pointsTotal(input: string[]) {
  return input.map(parseCard).map(findWinning).map(calculatePoints).reduce(sum);
}

function cardsTotal(input: string[]) {
  return Array.from(
    input
      .map(parseCard)
      .reduce((counts, card, index) => {
        if (!counts.has(index)) {
          counts.set(index, 1);
        }
        for (let z = index + 1; z < index + findWinning(card).length + 1; z++) {
          counts.set(z, (counts.get(z) || 1) + counts.get(index));
        }
        return counts;
      }, new Map())
      .values()
  ).reduce(sum);
}

function sum(total: number, value: number) {
  return total + value;
}
function calculatePoints(winning: number[]) {
  return winning.length > 0 ? Math.pow(2, winning.length - 1) : 0;
}

function findWinning(card: Card) {
  return card.available.flatMap((available) =>
    card.winning.filter((winning) => winning === available)
  );
}

function parseCard(row: string) {
  const [card, values] = row.split(":");
  const [winning, available] = values
    .trim()
    .split("|")
    .map((list) =>
      list
        .trim()
        .split(/\s+/)
        .map((num) => parseInt(num.trim()))
        .filter((num) => !isNaN(num))
    );
  return {
    card,
    winning,
    available,
  };
}

export { pointsTotal, cardsTotal };
