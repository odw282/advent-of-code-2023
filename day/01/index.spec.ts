import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { calibrate, calibrateDigits } from "./index";
const values = readFileIntoArray(path.join(__dirname, "./input.txt"));
describe("Day 1: Trebuchet?!", () => {
  test("Test Case A", () => {
    const answer = calibrate([
      "1abc2",
      "pqr3stu8vwx",
      "a1b2c3d4e5f",
      "treb7uchet",
    ]);
    expect(answer).toEqual(142);
  });

  test("Solution A", () => {
    const answer = calibrate(values);
    expect(answer).toEqual(56465);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = calibrateDigits([
      "two1nine",
      "eightwothree",
      "abcone2threexyz",
      "xtwone3four",
      "4nineeightseven2",
      "zoneight234",
      "7pqrstsixteen",
    ]);
    expect(answer).toEqual(281);
  });

  it("Solution B", () => {
    const answer = calibrateDigits(values);
    expect(answer).toEqual(55902);
    console.info("B:", answer);
  });
});
