"use strict";

// @ts-ignore
import lcm from "compute-lcm";

const LEFT = 0;
const RIGHT = 1;

type Node = [string, string]; // lEFT, RIGHT

function parseMap(input: string[]) {
  const [lr, , ...wasteland] = input;
  const instructions = lr.split("").map((i) => (i === "L" ? LEFT : RIGHT));
  const nodes = new Map<string, Node>(
    wasteland.map((node) => {
      const [n, dir] = node.split(" = ");
      return [n, dir.substring(1, dir.length - 1).split(", ") as Node];
    })
  );
  return { instructions, nodes };
}

function pathSteps(input: string[]) {
  const { instructions, nodes } = parseMap(input);
  let target = "AAA",
    steps = 0;
  while (target !== "ZZZ") {
    target = nodes.get(target)[instructions[steps % instructions.length]];
    steps++;
  }
  return steps;
}

function parallelPathSteps(input: string[]) {
  const { instructions, nodes } = parseMap(input);
  let targets = Array.from(nodes.keys()).filter((node) => node[2] === "A");
  return lcm(
    targets.map((target) => {
      let t = target,
        steps = 0;
      while (t[2] !== "Z") {
        const dir = instructions[steps % instructions.length];
        t = nodes.get(t)[dir];
        steps++;
      }
      return steps;
    })
  );
}

export { pathSteps, parallelPathSteps };
