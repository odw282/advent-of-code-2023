"use strict";

type CharMap = { [key: string]: string };
type HandType = [string, number, number, string];

const SORT_RANK_INDEX = 2;
const SORT_TYPE_INDEX = 3;
const LETTER_MAP: CharMap = {
  A: "E",
  K: "D",
  Q: "C",
  J: "B",
  T: "A",
  "9": "9",
  "8": "8",
  "7": "7",
  "6": "6",
  "5": "5",
  "4": "4",
  "3": "3",
  "2": "2",
};
const JOKER_LETTER_MAP: CharMap = {
  ...LETTER_MAP,
  J: "1",
};
const KEYS = ["A", "K", "Q", "T", "9", "8", "7", "6", "5", "4", "3", "2"];

// [original hand, bid, value, sort, joker_sort]
const handType = ([hand, bid]: [string, string]): HandType => [
  hand,
  parseInt(bid),
  value(hand),
  hand
    .split("")
    .map((char) => LETTER_MAP[char] || char)
    .join(""),
];

const jokerHandType = ([hand, bid]: [string, string]): HandType => [
  hand,
  parseInt(bid),
  valueWithJoker(hand),
  hand
    .split("")
    .map((char) => JOKER_LETTER_MAP[char] || char)
    .join(""),
];

function totalWinnings(input: string[]) {
  return input
    .map((line) => line.split(/\s+/))
    .map(handType)
    .sort(sortRank)
    .reduce((total, card, index) => total + card[1] * ++index, 0);
}

function totalWinningsWithJoker(input: string[]) {
  return input
    .map((line) => line.split(/\s+/))
    .map(jokerHandType)
    .sort(sortRank)
    .reduce((total, card, index) => total + card[1] * ++index, 0);
}

function sortRank(a: HandType, b: HandType) {
  return a[SORT_RANK_INDEX] - b[SORT_RANK_INDEX] || sortHand(a, b);
}

function sortHand(a: HandType, b: HandType) {
  return a[SORT_TYPE_INDEX] < b[SORT_TYPE_INDEX]
    ? -1
    : a[SORT_TYPE_INDEX] > b[SORT_TYPE_INDEX]
    ? 1
    : 0;
}

function recurseHand(hand: string[]): number {
  const index = hand.indexOf("J");
  if (index != -1) {
    return KEYS.reduce((max, char) => {
      const nextHand = hand.toSpliced(index, 1, char);
      const rank = nextHand.includes("J")
        ? recurseHand(nextHand)
        : value(nextHand.join(""));
      return Math.max(max, rank);
    }, 0);
  }
  return value(hand.join());
}

function valueWithJoker(hand: string) {
  if (hand.split("").includes("J")) {
    return recurseHand(hand.split(""));
  } else {
    return value(hand);
  }
}

function value(hand: string) {
  const counts = hand
    .split("")
    .map((char, index, hand) => hand.filter((c) => c === char).length);

  if (counts.includes(5)) {
    return 6;
  }
  if (counts.includes(4)) {
    return 5;
  }
  if (counts.includes(3)) {
    if (counts.includes(2)) {
      return 4;
    }
    return 3;
  }
  if (counts.filter((i) => i === 2).length === 4) {
    return 2;
  }
  if (counts.includes(2)) {
    return 1;
  }
  return 0;
}

export { totalWinnings, totalWinningsWithJoker };
