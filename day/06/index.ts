"use strict";
import { tap } from "../../utils/tap";

function boatRace(input: string[]) {
  const [times, distances] = input
    .map((line) => line.split(": ")[1])
    .map((part) => part.trim().split(/\s+/).map(x => parseInt(x)));
  return times.map(wins(distances)).reduce(mul);
}

function singleRace(input: string[]) {
  const [times, distances] = input
      .map((line) => line.split(": ")[1])
      .map((part) => [parseInt(part.replace(/\s+/g, "").trim())]);
  return times.map(wins(distances)).reduce(mul);
}

const wins = (distances: number[]) => (time: number, index: number) => {
  let wins = [];
  for (let hold = 1; hold < time; hold++) {
    if (hold * (time - hold) > distances[index]) {
      wins.push(hold);
    }
  }
  return wins.length;
};

const mul = (total: number, value: number) => total * value;

export { boatRace, singleRace };
