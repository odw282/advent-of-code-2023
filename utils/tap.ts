import {isArray} from "lodash";

export function tap(value: any) {
    if (isArray(value)) {
        console.log(value.join(""))
    } else {
        console.log(value);
    }
    return value;
}