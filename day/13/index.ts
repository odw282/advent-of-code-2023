"use strict";
import { zip } from "lodash";

function findMirror(block: string[]) {
  for (let i = 1; i < block.length; i++) {
    let above = block.slice(0, i).reverse();
    let below = block.slice(i);
    above = above.slice(0, below.length);
    below = below.slice(0, above.length);
    if (above.every((row, i) => row == below[i])) {
      return i;
    }
  }
  return 0;
}

function findMirrorAlt(block: string[]) {
  for (let i = 1; i < block.length; i++) {
    let above = block.slice(0, i).reverse();
    let below = block.slice(i);
    if (
      zip(above, below).reduce((total, [x, y]) => {
        if (x === undefined || y === undefined) return total;
        return (
          total +
          zip(x.split(""), y.split("")).reduce(
            (total, [a, b]) => total + (a === b ? 0 : 1),
            0
          )
        );
      }, 0) === 1
    ) {
      return i;
    }
  }
  return 0;
}

function sumReflections(input: string[]) {
  const blocks = input.map((block) => block.split("\n"));
  const rows = blocks.reduce(
    (total, block) => total + findMirror(block) * 100,
    0
  );
  const cols = blocks.reduce(
    (total, block) =>
      total +
      findMirror(zip(...block.map((r) => r.split(""))).map((l) => l.join(""))),
    0
  );
  return rows + cols;
}

function sumReflectionsAlt(input: string[]) {
  const blocks = input.map((block) => block.split("\n"));
  const rows = blocks.reduce(
    (total, block) => total + findMirrorAlt(block) * 100,
    0
  );
  const cols = blocks.reduce(
    (total, block) =>
      total +
      findMirrorAlt(
        zip(...block.map((r) => r.split(""))).map((l) => l.join(""))
      ),
    0
  );
  return rows + cols;
}

export { sumReflections, sumReflectionsAlt };
