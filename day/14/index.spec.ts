import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {loadAfterCycle, totalLoad} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
  "O....#....",
  "O.OO#....#",
  ".....##...",
  "OO.#O....O",
  ".O.....O#.",
  "O.#..O.#.#",
  "..O..#O..O",
  ".......O..",
  "#....###..",
  "#OO..#....",
];

describe("Day 14: Parabolic Reflector Dish", () => {
  test("Test Case A", () => {
    const answer = totalLoad(testInput);
    expect(answer).toEqual(136);
  });

  test("Solution A", () => {
    const answer = totalLoad(input)
    expect(answer).toEqual(108759);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = loadAfterCycle(testInput, 1000000000);
    expect(answer).toEqual(64);
  });

  it("Solution B", () => {
    const answer = loadAfterCycle(input, 1000000000)
    expect(answer).toEqual(89089);
    console.info("B:", answer);
  });
});
