import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {totalWinnings, totalWinningsWithJoker} from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
    "32T3K 765",
    "T55J5 684",
    "KK677 28",
    "KTJJT 220",
    "QQQJA 483"
];
describe("Day 7: Camel Cards", () => {

    test("Test Case A", () => {
      const answer = totalWinnings(testInput);
      expect(answer).toEqual(6440);
    });

    test("Solution A", () => {
      const answer = totalWinnings(input);
      expect(answer).toEqual(248179786);
      console.info("A:", answer);
    });

    test("Test Case B", () => {
      const answer = totalWinningsWithJoker(testInput)
      expect(answer).toEqual(5905);
    });

    it("Solution B", () => {
      const answer = totalWinningsWithJoker(input)
      expect(answer).toEqual("B");
      console.info("B:", answer);
    });

});
