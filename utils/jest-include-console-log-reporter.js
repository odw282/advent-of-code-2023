const {
  specialChars: { ICONS },
} = require("jest-util");
const { VerboseReporter } = require("@jest/reporters");
const chalk = require("chalk");

/**
 * To stop cluttering the terminal with console output containing the answers we need
 * use the console.info command to send answers to the end of test result.
 */
class JestIncludeConsoleLogReporter extends VerboseReporter {
  constructor(globalConfig) {
    super(globalConfig);
  }

  /**
   * @override
   * @param status
   * @return {*}
   * @private
   */
  _getIcon(status) {
    if (status === "answer") {
      return chalk.magenta(ICONS.todo);
    }
    return super._getIcon(status);
  }

  /**
   * @override
   * @param testPath
   * @param config
   * @param result
   */
  printTestFileHeader(testPath, config, result) {
    // console will be undefined if no messages are preset
    result.console = result.console === undefined ? [] : result.console;
    // Use console info type to display answers for our tests
    if (result.testResults.length) {
      // add new test result
      result.testResults.push({
        ancestorTitles: result.testResults[0].ancestorTitles,
        duration: 1,
        failureDetails: [],
        failureMessages: [],
        fullName: "",
        invocations: 1,
        location: null,
        numPassingAsserts: 0,
        status: "answer",
        title: `Answers: ${chalk.greenBright(
          result.console
            .filter((c) => c.type === "info")
            .map((c) => c.message)
            .join(" ")
        )}`,
      });
    }

    // Print all console types expect 'info'
    super.printTestFileHeader(testPath, config, {
      ...result,
      console: this.filterConsoleType(result),
    });
  }

  /**
   * Filter console messages, needs to be undefined when length is zero
   * @param result
   * @return {string[] | undefined}
   */
  filterConsoleType(result) {
    const lines = result.console.filter((c) => c.type !== "info");
    if (lines.length === 0) return undefined;
    else return lines;
  }
}

module.exports = JestIncludeConsoleLogReporter;
