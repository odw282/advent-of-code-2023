import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { predict, predictBackwards } from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = ["0 3 6 9 12 15", "1 3 6 10 15 21", "10 13 16 21 30 45"];
describe("Day 9: Mirage Maintenance", () => {
  test("Test Case A", () => {
    const answer = predict(testInput);
    expect(answer).toEqual(114);
  });

  test("Solution A", () => {
    const answer = predict(input);
    expect(answer).toEqual(1789635132);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = predictBackwards(testInput);
    expect(answer).toEqual(2);
  });

  it("Solution B", () => {
    const answer = predictBackwards(input)
    expect(answer).toEqual(913);
    console.info("B:", answer);
  });
});
