"use strict";

import { unzip, zip } from "lodash";
import { tap } from "../../utils/tap";
import * as console from "console";

type Node = {
  nr: number;
  index: number;
  x: number;
  y: number;
  dx: number;
  dy: number;
  galaxy: boolean;
};

type Map = Node[][];

const GALAXY = "#";

function sumOfInfinite(input: string[], extraSpace = 999999) {
  let raw = toArray(input);
  const { rows, cols } = emptyCount(raw);
  //console.log({rows, cols})
  let map = parse(raw);
  let galaxies = new Map(
      map.flatMap((row) => row.filter((node) => node.galaxy)).entries()
  );
  let pairs = new Set<[Node, Node]>();
  for (let i = 0; i < galaxies.size; i++) {
    for (let j = i; j < galaxies.size; j++) {
      if (i !== j) {
        pairs.add([galaxies.get(i), galaxies.get(j)]);
      }
    }
  }
//print(map)
  const nrs = Array.from(pairs)
      .map(([a, b]) => {
        const dist = manhattan(a,b);
        const crosses = nrCrosses(rows, cols, [a,b])
        const distance = dist + (crosses * extraSpace);
        return [a, b, distance]
      })

      .map((d) => d[2] as number)



  const pair = Array.from(pairs)[1]
  const dist = manhattan(pair[0],pair[1])
  const crosses = nrCrosses(rows, cols, pair)
  const distance = dist + (crosses * extraSpace);
  //console.log({pair, dist , crosses, distance});
  return nrs.reduce((total, value) => total + value, 0);

}

function nrCrosses(rows: number[], cols: number[], pair: Node[]) {

  function rowCrosses(rows: number[], pair: Node[]) {
    const [a, b] = pair.sort((a, b) => a.x - b.x);
    let crosses = 0;
    for (let dx = a.x; dx < b.x; dx++) {
      if (cols.includes(dx)) {
        crosses++;
      }
    }
    return crosses;
  }

  function colCrosses(rows: number[], pair: Node[]) {
    const [a, b] = pair.sort((a, b) => a.y - b.y);

    let crosses = 0;
    for (let dy = a.y; dy < b.y; dy++) {
      if (rows.includes(dy)) {
        crosses++;
      }
    }
    return crosses;
  }

  return rowCrosses(rows, pair) + colCrosses(rows, pair);
}

function sumOfLength(input: string[]) {
  let raw = toArray(input);
  let map = parse(expand(raw));
  let galaxies = new Map(
    map.flatMap((row) => row.filter((node) => node.galaxy)).entries()
  );
  let pairs = new Set<[Node, Node]>();
  for (let i = 0; i < galaxies.size; i++) {
    for (let j = i; j < galaxies.size; j++) {
      if (i !== j) {
        pairs.add([galaxies.get(i), galaxies.get(j)]);
      }
    }
  }
  const nrs = Array.from(pairs)
    .map(([a, b]) => [a, b, manhattan(a, b)])
    .map((d) => d[2] as number)
  //console.log(nrs.join(" "));
    return nrs.reduce((total, value) => total + value, 0);
}

function parse(input: string[][]): Map {
  let nr = 0;
  return input.map((row, y) =>
    row.map(
      (ch, x) =>
        ({
          nr: ch === GALAXY ? ++nr : -1,
          index: y * row.length + x,
          x,
          y,
          dx: x + 1,
          dy: y + 1,
          galaxy: ch === GALAXY,
        } as Node)
    )
  );
}

function print(map: Map) {
  console.log(
    map
      .map((row, y) => row.map((node, x) => (node.galaxy ? node.nr : ".")).join(""))
      .join("\n")
  );
}

function emptyCount(raw: string[][]) {
  const rows = raw.flatMap((row, index) => row.includes('#') ? [] : [index]);
  const cols = zip(...raw).flatMap((row, index) => row.includes('#') ? [] : [index]);
  return {
    rows, cols
  }
}

function manhattan(a: Node, b: Node) {
  return Math.abs(b.dx - a.dx) + Math.abs(b.dy - a.dy);
}

function toArray(input: string[]) {
  return input.map((row) => row.split(""));
}

function expand(map: string[][]) {
  function _exp(map: string[][]) {
    return map.flatMap((row) =>
      row.some((x) => x === GALAXY) ? [row] : [row, row]
    );
  }
  return zip(..._exp(zip(..._exp(map))));
}

export { sumOfLength, sumOfInfinite };
