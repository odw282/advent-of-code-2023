"use strict";
import Denque from "denque";
import "core-js/proposals/set-methods-v2.js";

const GROUND = ".";
const START = "S";

type Node = {
  index: number;
  from: Node[];
  to: Node[];
  row: number;
  col: number;
  pipe: string;
};

type Map = Node[][];

const UP = 0;
const DOWN = 3;
const LEFT = 1;
const RIGHT = 2;

const dir = [
  [0, -1],
  [-1, 0],
  [1, 0],
  [0, 1],
];

type AllowedMapping = {
  [key: string]: {
    [key: string]: string[] | null;
  };
};

const allowedMapping: AllowedMapping = {
  [UP]: {
    "|": ["|", "F", "7", "S"],
    "-": null,
    L: ["|", "F", "7", "S"],
    J: ["|", "F", "7", "S"],
    F: null,
    "7": null,
    ".": null,
    S: ["|", "F", "7"],
  },
  [DOWN]: {
    "|": ["|", "L", "J", "S"],
    "-": null,
    L: null,
    J: null,
    F: ["|", "L", "J", "S"],
    "7": ["|", "L", "J", "S"],
    ".": null,
    S: ["|", "L", "J"],
  },
  [LEFT]: {
    "|": null,
    "-": ["-", "L", "F", "S"],
    L: null,
    J: ["-", "L", "F", "S"],
    F: null,
    "7": ["-", "L", "F", "S"],
    ".": null,
    S: ["-", "L", "F"],
  },
  [RIGHT]: {
    "|": null,
    "-": ["-", "J", "7", "S"],
    L: ["-", "J", "7", "S"],
    J: null,
    F: ["-", "J", "7", "S"],
    "7": null,
    ".": null,
    S: ["-", "J", "7"],
  },
};

function totalEnclosed(input: string[]) {
  const { seen: loop, map, start, replacement } = maxDistance(input);
  const outside = new Set<Node>();
  start.pipe = Array.from(replacement.values())[0];
  map.forEach((row) =>
    row.forEach((node) => (node.pipe = loop.has(node) ? node.pipe : GROUND))
  );
  for (let y = 0; y < map.length; y++) {
    let within = false,
      up: boolean | null = null;
    for (let x = 0; x < map[0].length; x++) {
      const node = map[y][x];
      if (node.pipe === "|") {
        if (up === null) {
          within = !within;
        }
      } else if (node.pipe === "-") {
        // do nothing
      } else if ("FL".includes(node.pipe)) {
        up = node.pipe === "L";
      } else if ("7J".includes(node.pipe)) {
        if (up !== null) {
          if (node.pipe !== (up ? "J" : "7")) {
            within = !within;
          }
          up = null;
        }
      } else if (".".includes(node.pipe)) {
        //
      } else {
        throw new Error(`pipe not found: ${node.pipe}`);
      }

      if (!within) {
        outside.add(node);
      }
    }
  }
  const inside = map.flatMap((row) =>
    row
      .filter((node) => !outside.has(node) && !loop.has(node))
      .flatMap((node) => node)
  );
  return inside.length;
}

function maxDistance(input: string[]) {
  const map = parseMap(input);
  const start = getNodeByIndex(map, findStartIndex(map));
  const seen = new Set<Node>();
  let replacement = new Set<string>(["|", "-", "J", "L", "7", "F"]);
  seen.add(start);
  const q = new Denque<Node>([start]);
  while (!q.isEmpty()) {
    const node = q.shift();
    dir.forEach(([dx, dy], direction) => {
      const y = node.row + dy;
      const x = node.col + dx;
      const allowedMap = allowedMapping[direction][node.pipe];
      const onMap = y >= 0 && y < map.length && x >= 0 && x < map[0].length;
      if (onMap && allowedMap) {
        const next = map[y][x];
        if (allowedMap.includes(next.pipe) && !seen.has(next)) {
          seen.add(next);
          q.push(next);
          if (node.pipe === START) {
            // @ts-ignore
            replacement = replacement.intersection(new Set(allowedMap));
          }
        }
      }
    });
  }
  return { map, seen, start, replacement };
}

function getMaxDistance(input: string[]) {
  const { seen } = maxDistance(input);
  return Array.from(seen.values()).length / 2;
}

function parseMap(input: string[]): Map {
  return input.map((row, i) =>
    row.split("").map((col, j) => {
      const index = j + i * input[0].length;
      return {
        index,
        from: [],
        to: [],
        row: i,
        col: j,
        pipe: col,
      } as Node;
    })
  );
}

const getNodeByIndex = (map: Map, index: number) => {
  const y = Math.floor(index / map[0].length);
  const x = Math.ceil(index % map[0].length);
  if (y >= 0 && y < map.length && x >= 0 && x < map[0].length) {
    return map[y][x];
  }
};

function findStartIndex(map: Map): number {
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[y].length; x++) {
      if (map[y][x].pipe === START) {
        return y * map[0].length + x;
      }
    }
  }
  return -1;
}

export { getMaxDistance, totalEnclosed };
