import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import { parallelPathSteps, pathSteps } from "./index";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
  "LLR",
  "",
  "AAA = (BBB, BBB)",
  "BBB = (AAA, ZZZ)",
  "ZZZ = (ZZZ, ZZZ)",
];
const testInput2 = [
  "LR",
  "",
  "11A = (11B, XXX)",
  "11B = (XXX, 11Z)",
  "11Z = (11B, XXX)",
  "22A = (22B, XXX)",
  "22B = (22C, 22C)",
  "22C = (22Z, 22Z)",
  "22Z = (22B, 22B)",
  "XXX = (XXX, XXX)",
];

describe("Day 8: Haunted Wasteland", () => {
  test("Test Case A", () => {
    const answer = pathSteps(testInput);
    expect(answer).toEqual(6);
  });

  test("Solution A", () => {
    const answer = pathSteps(input);
    expect(answer).toEqual(19241);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = parallelPathSteps(testInput2);
    expect(answer).toEqual(6);
  });

  it("Solution B", () => {
    const answer = parallelPathSteps(input);
    expect(answer).toEqual(9606140307013);
    console.info("B:", answer);
  });
});
