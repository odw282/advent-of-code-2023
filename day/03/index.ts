"use strict";

import { tap } from "../../utils/tap";

type PartIndex = {
  index: number;
  start: number;
  end: number;
};

type Part = PartIndex & {
  value: number;
};

type Gear = PartIndex & {
  value: string;
};

type TouchingPart = Part & {
  isTouching: boolean;
};

const find =
  <T>(finder: (char: string) => boolean) =>
  (line: string[], index: number): T[] => {
    let start = 0,
      end = 0,
      value: number | string = 0;
    const numbers: T[] = [];
    let i = 0;
    while ((start = findStart(finder, line, end)) !== null) {
      const next = line.slice(start).findIndex((char) => !finder(char));
      if (next !== -1) {
        end = start + next;
      } else {
        end = start + line.slice(start).length;
      }
      const partString = line.slice(start, end).join("");
      value = parseInt(partString);
      if (isNaN(value)) {
        value = partString;
      }
      numbers.push({ index, start, end, value } as unknown as T);
      i++;
    }
    return numbers;
  };

const findPartNumbers = find<Part>((char) => isNumber(char));
const findGears = find<Gear>((char) => isGear(char));
const isNumber = (char: string) => !isNaN(parseInt(char));
const isPeriod = (char: string) => char === ".";
const isGear = (char: string) => char === "*";

const isSymbol = (char: string) => !isPeriod(char) && !isNumber(char);

const findStart = (
  finder: (char: string) => boolean,
  line: string[],
  from: number = 0
) => {
  const start = line.slice(from).findIndex(finder);
  return start === -1 ? null : from + start;
};

const isTouchingGear =
  (engine: string[][], gear: Gear) =>
  ({ index, start, end, value }: Part): TouchingPart => {
    let isTouching = false;
    for (let x = start - 1; x <= end; x++) {
      for (let y = index - 1; y <= index + 1; y++) {
        try {
          const char = engine[y][x];
          if (char && isGear(char) && x === gear.start && y === gear.index) {
            isTouching = true;
            break;
          }
        } catch (e) {
          //
        }
      }
    }
    return { index, start, end, value, isTouching };
  };
const isTouchingSymbol =
  (engine: string[][]) =>
  ({ index, start, end, value }: Part): TouchingPart => {
    let isTouching = false;
    for (let x = start - 1; x <= end; x++) {
      for (let y = index - 1; y <= index + 1; y++) {
        try {
          const char = engine[y][x];
          if (char && isSymbol(char)) {
            isTouching = true;
            break;
          }
        } catch (e) {
          //
        }
      }
    }
    return { index, start, end, value, isTouching };
  };

function sumGearRatio(input: string[]) {
  const engine = input.map((line) => line.split(""));
  return engine
    .flatMap(findGears)
    .map((gear) =>
      engine
        .flatMap(findPartNumbers)
        .map(isTouchingGear(engine, gear))
        .filter(({ isTouching }) => isTouching)
    )
    .filter((touchingPart) => touchingPart.length === 2)
    .map((t) => t[0].value * t[1].value)
    .reduce((total, value) => total + value, 0);
}

function sumPartNumbers(input: string[]) {
  const engine = input.map((line) => line.split(""));
  return engine
    .flatMap(findPartNumbers)
    .map(isTouchingSymbol(engine))
    .filter(({ isTouching }) => isTouching)
    .reduce((total, { value }) => total + value, 0);
}

export { sumPartNumbers, sumGearRatio };
