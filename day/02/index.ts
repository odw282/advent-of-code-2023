"use strict";

function setPower(games: string[]) {
  return games
    .map(parseGame)
    .map((game) =>
      game.subsets.reduce(
        (max, row) => row.map((i, idx) => Math.max(i, max[idx])),
        [0, 0, 0] as [number, number, number]
      )
    )
    .map((set) => set.reduce((total, val) => total * val, 1))
    .reduce((total, val) => total + val, 0);
}

function gamesPossible(games: string[], config = [12, 13, 14]) {
  return games
    .map(parseGame)
    .map((game) => {
      const isPossible = game.subsets.every((set) =>
        set.every((val, idx) => val <= config[idx])
      );
      return {
        game,
        isPossible,
      };
    })
    .filter(({ isPossible }) => isPossible)
    .reduce((total, { game }) => total + game.id, 0);
}

const SORT_ORDER = { red: 0, green: 1, blue: 2 } as { [key: string]: number };
function parseGame(game: string) {
  const [gameId, sets] = game.split(":");
  const subsets = sets.split(";").map((set) =>
    set
      .split(",")
      .map((color) => color.trim().split(" "))
      .reduce(
        (final, row) => {
          final[SORT_ORDER[row[1]]] = parseInt(row[0]);
          return final;
        },
        [0, 0, 0] as [number, number, number]
      )
  );
  return {
    id: parseInt(gameId.replace("Game ", "")),
    subsets,
  };
}

export { gamesPossible, setPower };
