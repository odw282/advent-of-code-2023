import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";
import {sumGearRatio, sumPartNumbers} from "./index";

const parts = readFileIntoArray(path.join(__dirname, "./input.txt"));
const testInput = [
  "467..114..",
  "...*......",
  "..35..633.",
  "......#...",
  "617*......",
  ".....+.58.",
  "..592.....",
  "......755.",
  "...$.*....",
  ".664.598..",
];

describe("Day 3: Gear Ratios", () => {
  test("Test Case A", () => {
    const answer = sumPartNumbers(testInput);
    expect(answer).toEqual(4361);
  });

  test("Solution A", () => {
    const answer = sumPartNumbers(parts);
    expect(answer).toEqual(546312);
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = sumGearRatio(testInput)
    expect(answer).toEqual(467835);
  });

  it("Solution B", () => {
    const answer = sumGearRatio(parts)
    expect(answer).toEqual(87449461);
    console.info("B:", answer);
  });
});
