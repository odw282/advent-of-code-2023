"use strict";
import { unzip, zip } from "lodash";
import * as console from "console";

const ROUND_ROCK = "O";
function totalLoad(input: string[]) {
  const grid = input.map((row) => row.split(""));
  return unzip(zip(...grid).map(roll)).reduce(calcLoad, 0);
}

function loadAfterCycle(input: string[], cycle = 1) {
  let grid = input.map((row) => row.split(""));
  let j = 0;
  const loads = [];
  const grids = [grid];
  const seen = new Set([serialize(grid)]);

  for (let i = 0; i < cycle; i++) {
    j++;
    grid = unzip(
      zip(
        ...unzip(zip(...grid).map(roll))
          .map(roll)
          .reverse()
      ).map(roll)
    )
      .reverse()
      .map((row) => row.reverse())
      .map(roll)
      .map((row) => row.reverse());

    const g = serialize(grid);

    if (seen.has(g)) {
      break;
    }

    seen.add(g);
    grids.push(grid);
    loads.push(grid.reduce(calcLoad, 0));
  }
  const first = grids.findIndex((g, index) => serialize(g) === serialize(grid));
  return grids[((cycle - first) % (j - first)) + first].reduce(calcLoad, 0);
}

const serialize = (grid: string[][]) =>
  grid.map((row) => row.join("")).join("");

const roll = (row: string[]) =>
  row
    .join("")
    .split("#")
    .map((part) => part.split("").sort().reverse().join(""))
    .join("#")
    .split("");

const calcLoad = (
  total: number,
  row: string[],
  rowIndex: number,
  arr: string[][]
) =>
  total +
  row.reduce(
    (total, cell, cellIndex) =>
      total + (cell === ROUND_ROCK ? arr.length - rowIndex : 0),
    0
  );

export { totalLoad, loadAfterCycle };
