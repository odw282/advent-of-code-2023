import { readFileIntoArray } from "../../utils/fs";
import * as path from "path";

const input = readFileIntoArray(path.join(__dirname, "./input.txt"));
describe("Day 16: The Floor Will Be Lava", () => {
  test("Test Case A", () => {
    const answer = "A";
    expect(answer).toEqual("A");
  });

  test("Solution A", () => {
    const answer = "A";
    expect(answer).toEqual("A");
    console.info("A:", answer);
  });

  test("Test Case B", () => {
    const answer = "B";
    expect(answer).toEqual("B");
  });

  it("Solution B", () => {
    const answer = "B";
    expect(answer).toEqual("B");
    console.info("B:", answer);
  });
});
